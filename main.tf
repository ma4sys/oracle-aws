provider "aws" {
	region = "us-east-1"
}

module "vpc" {
	source = "terraform-aws-modules/vpc/aws"

	name = "my-vpc"
	cidr = var.vpc_cidr_block

	azs             = [var.avail_zone]
	public_subnets  = [var.subnet_cidr_block]
	public_subnet_tags = { Name = "${var.env_prefix}-subnet-1" }

	tags = {
			Name = "${var.env_prefix}-vpc"
	}
}